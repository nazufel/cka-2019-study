# terraform/k8s-workers.tf

# source = https://www.terraform.io/docs/providers/do/d/droplet.html

# build Kubernetes workers 

# Create a new tag
resource "digitalocean_tag" "worker" {
  name = "worker"
}


# Create a new Droplets in the nyc1 region
resource "digitalocean_droplet" "worker" {
  # note: practicing with bootstrapping clusters based on Ubuntu and CEntOS.  
  # image  = "centos-7-x64"
  image  = "ubuntu-18-04-x64"
  name   = "k8sworker-${count.index + 1}"
  region = "nyc1"
  size   = "s-4vcpu-8gb"
  ssh_keys = ["${var.ssh_fingerprint}"]
  tags   = [digitalocean_tag.kubernetes.id,digitalocean_tag.worker.id]
  count = 3
}

