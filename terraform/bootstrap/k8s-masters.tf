# terraform/k8s-masters.tf

# source = https://www.terraform.io/docs/providers/do/d/droplet.html

# Create a new tag
resource "digitalocean_tag" "master" {
  name = "master"
}

# build Kubernetes masters 

# Create a new Web Droplet in the nyc1 region
resource "digitalocean_droplet" "master" {
  # note: practicing with bootstrapping clusters based on Ubuntu and CEntOS.
  # image  = "centos-7-x64"
  image  = "ubuntu-18-04-x64"
  name   = "k8smaster-${count.index + 1}"
  region = "nyc1"
  size   = "s-4vcpu-8gb"
  ssh_keys = ["${var.ssh_fingerprint}"] 
  count = 3
  tags   = [digitalocean_tag.master.id, digitalocean_tag.kubernetes.id]
}

