# terraform/variables.tf

# vars for terraform

variable "ssh_fingerprint" {}

# Create a tag - https://www.terraform.io/docs/providers/do/r/tag.html
resource "digitalocean_tag" "kubernetes" {
  name = "kubernetes"
}