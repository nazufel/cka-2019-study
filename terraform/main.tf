# terraform/provider.tf

# source = https://www.terraform.io/docs/providers/do/index.html#digitalocean_token

# set the providor for this terrform config

# set api token from environment variable in accordance with 12 Factor App

module "bootstrap" {
  source = "./bootstrap"
  name = "bootstrap"
}

module "doks" {
  source = "./doks"
  name = "doks"
}