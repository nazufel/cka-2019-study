# Terraform Readme

Terraform is used to create resources in cloud providers. This structure is broken into modules.

## Accessing Digital Ocean with Terraform

Terraform needs an API key to talk to Digital Ocean (DO) api. I created an API key for my DO account and saved it as an environment variable on my laptop. This [link](https://www.terraform.io/docs/commands/environment-variables.html) describes the format of how to create the environment variable.

## Bootstrap

Exactly what it sounds like. It creates a bunch of servers to then be manually bootstrapped for kubeadm, kube masters, kubelet, and etcd practice.

## DOKS

Stands up a DO managed cluster when I don't need to practice kubeadm. I did this most of the time to practice the Kubernetes api primitives.

## Envoking Terraform

Terraform modules can be ran from where they are on the file system.
1. cd into the directory you want to build, ```bootstrap``` or ```doks```. This example will use the doks module.
```sh
$ cd doks
```
2. Init the terraform module
```sh
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "kubernetes" (hashicorp/kubernetes) 1.10.0...
- Downloading plugin for provider "digitalocean" (terraform-providers/digitalocean) 1.12.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.digitalocean: version = "~> 1.12"
* provider.kubernetes: version = "~> 1.10"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
3. Run the terraform plan command to see the 
```sh
$ terraform plan 
...
Plan: 1 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```
4. Apply the Terraform module
```sh
$ terraform apply                                                                                                                                               (git)-[master] 

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # digitalocean_kubernetes_cluster.cka will be created
  + resource "digitalocean_kubernetes_cluster" "cka" {
      + cluster_subnet = (known after apply)
      + created_at     = (known after apply)
      + endpoint       = (known after apply)
      + id             = (known after apply)
      + ipv4_address   = (known after apply)
      + kube_config    = (sensitive value)
      + name           = "cka"
      + region         = "nyc1"
      + service_subnet = (known after apply)
      + status         = (known after apply)
      + tags           = [
          + "cka",
          + "practice",
        ]
      + updated_at     = (known after apply)
      + version        = "1.16.2-do.2"

      + node_pool {
          + actual_node_count = (known after apply)
          + auto_scale        = false
          + id                = (known after apply)
          + name              = "worker-pool"
          + node_count        = 3
          + nodes             = (known after apply)
          + size              = "s-2vcpu-2gb"
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

digitalocean_kubernetes_cluster.cka: Creating...

...

digitalocean_kubernetes_cluster.cka: Still creating... [5m51s elapsed]
digitalocean_kubernetes_cluster.cka: Creation complete after 5m56s [id=***]
```

5. Add the new cluster to your kubeconfig
```
$ doctl kube cluster kubeconfig save <name of cluster>
```

Good luck practicing for the exam.