# Installation, Configuration, & Validation 12%

## Design a Kubernetes cluster
Not sure the exact outcome of this. There are masters and workers who need TLS, etcd, and networking...

## Install Kubernetes masters and nodes, including TLS boostraping
This happens in multiple stages:
1. [Install the container runtime](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)
2. [Install kubeadm on all systems (masters and workers) that will be members of the cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)
3. [Init the cluster and first master node](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)
    - Be sure to read the section about choosing a network solution before initializing the first master because certain network addons require cli args to be fed to kubeadm upon init, such as Calico: ```kubeadmin --pod-network-cidr=192.168.0.0/16```, before the network addon is even installed. See below how to instsall particular network addons.
    - Then follow the steps in this substep's parent about adding worker nodes. They won't show ```ready``` in ```kubectl get nodes -o wide``` until a network addon has been installed. See more on that below.
    - Bonus: kubeadm tokens are only valid for two hours or the string printed during the final setup may get lost in the history. Use: ```kubeadm create token --print-join-command``` to get the entire worker join command
## [Configure secure cluster communications](https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/)

## [Configure a Highly-Available Kubernetse cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/)
## [Know where to get the Kubernetes release binaries](https://kubernetes.io/docs/setup/release/)
## Provision underlying infrastructure to deploy Kubernetes
This is addressed in the [terraform](./terraform) directory with two directories in there. These are terraform modules that will either create raw Ubuntu [Droplets](https://www.digitalocean.com/products/droplets/) for kubeadm bootstraping practice or use Digital Ocean's managed [Kubernetes service](https://www.digitalocean.com/products/kubernetes/)
## [Choose a network solution](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#pod-network)
Remember, some of network addons need special args ran passed to kubeadm, such as Calico. See above for that.
## Choose your Kubernetes infrastructure configuration
## [Run end-to-end tests on your cluster](https://github.com/kubernetes/test-infra/blob/master/kubetest/README.md)
## Analyse end-to-end test results
## Run Node end-to-end tests 
There isn't an automated way to test the cluster. Below are a few commands that should cover most usecases. All of these commands should return data and show no errors.
```sh
$ kubectl cluster-info
$ kubectl get nodes
$ kubectl get componentstatuses
$ kubectl get pods -o wide --show-labels --all-namespaces
$ kubectl get svc  -o wide --show-labels --all-namespaces
```

### [Install and use kubeadm to install, configure, and manage Kubernetes clusters](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm/)