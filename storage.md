# Storage 7%

## [Understand persistent volumes and how to create them](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes)

## Understand access modes for volumes

    - RWO: ReadWriteOnce- the volume can be mounted as read-write by a single node
    - ROX: ReadOnlyMany - the volume can be mounted read-only by many nodes
    - RWX: ReadWriteMany - the volume can be mounted as read-write by many nodes
## [Understand persistnt volumes clains primitive](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)
## [Undrstand Kubernetes storage objects](https://kubernetes.io/docs/concepts/storage/volumes/#types-of-volumes)
## [Know how to configure applications with persistent storage](https://kubernetes.io/docs/tasks/configure-pod-container/configure-volume-storage/#configure-a-volume-for-a-pod)

