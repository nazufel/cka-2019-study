# CKA 2019 Study

This study guide is meant to help pass the [CNCF](https://cncf.io) [Certificed Kuberenetes Administrator](https://www.cncf.io/certification/cka/) exam for CKA v1.14.1. The certification requirements can be found [here](https://github.com/cncf/curriculum). The procedures and commands will be based on those requirements.

## Disclaimer

This study guide comes with no warranty, support, or any guarantees. This was for me and maybe it can help you. I make no promises that you will be fit to pass the CKA or CKAD. I did, however, pass the CKA with these study notes.

## Exam Tips
* Using the documentation on [kubernetes.io](kubernetes.io) is allowed during the exam. Save bookmarks for each of the sections for reference in case you need it or in the case writing manifest because you don't want to write that out by hand.

## Study Environments
I used [Digital Ocean](https://www.digitalocean.com/) in practicing for the CKA. I built [Droplets](https://www.digitalocean.com/docs/droplets/) for practicing cluster boostraping and the other lower level practice. I then used [DOKS](https://www.digitalocean.com/docs/kubernetes/) to practice the api primitives. 

I used [Terraform](https://www.terraform.io/) to build out this infrastructure. You can find my Terraform configurations in the terraform directory.

## Parts of the Exam

### [Core Concepts](./core-concepts.md) - 19%
* Understand the Kubernetes API primitives
* Understand Kubernetes cluster architecture
* Understand Services and other network primitives 


### [Installation, Configuration, & Validation](./installation-configuration-validation.md) - 12%

* Design a Kubernetes cluster
* Install Kubernetes masters and nodes
* Configure secure cluster communications
* Configure a Highly-Available Kubernetes cluster
* Know where to get the Kubernetes release binaries
* Provision underlying infrastructure to deploy Kubernetes
* Choose a network solution
* Choose your Kubernetes infrastructure configuration
* Run end-to-end tests on your cluster
* Analyse end-to-end test results
* Run Node end-to-end tests 
* Install and use kubeadm to install, configure, and manage Kubernetes clusters

### [Security](./security.md) - 12% 
* Know how to configure authentication and authorization
* Understand Kubernetes security primitives
* Know to configure network policies
* Create and manage TLS certifiates for cluster components
* Work with images securely
* Define security contexts
* Secure persistent key value stores

### [Networking](./networking.md) - 11%
* Understand the networking configuration on the cluster nodes
* Understand Pod networking concepts
* Understand service networking
* Deploy and configure network load balancer
* Know how to use Ingress rules
* Know how to configure and use the cluster DNS
* Undrstand CNI

### [Cluster Maintenance](./cluster-maintenance.md) - 11%
* Understand the Kubernetes cluter upgrade process
* Facilitate operating system upgrades
* Impliment backup and restore methodologies

### [Troubleshooting](./troubleshooting.md) - 10%
* Troubleshoot application failure
* Troubleshoot control plane failure
* Troubleshoot worker node failure
* Troubleshoot networking

### [Application Lifecycle Management](./application-lifecycle-management.md) - 8% 
* Understand Deployments and how to perform rolling updates and rollbacks
* Know various ways to configure applications
* Know how to scale applications
* Understand the primitives necessary to create a self-healing application

### [Storage](./storage.md) - 7%
* Understand persistent volumes and how to create them 
* Understand access modes for volumes
* Understand persistent volumes clains primitive
* Undrstand Kubernetes storage objects
* Know how to configure applications with persistent storage 

### [Scheduling](./scheduling.md) 5%
* Use label selectors to schedule Pods
* Understand the role of DaemonSets
* Understand how resource limits can affect Pod schceduleing
* Manually schedule a pod without a scheduler
* Display scheduler events
* Know how to configure the Kubernetes scheduler


### [Logging/Monitoring](logging-monitoring.md) 5%
* Understand how to monitor all cluster components
* Understand how to monitor applications
* Manage cluster component logs
* Manage applicatoin logs 

## Contributing
PR's are very welcome. 

## Additional Resources
* [kubectl cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

## Acknowledgements
* [github.com/walidshaari](https://github.com/walidshaari/Kubernetes-Certified-Administrator/blob/master/README.md)
* Mumshad Manhembeth Udemy
* Linux Foundation Training