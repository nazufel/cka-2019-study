# Scheduling 5%

## [Use label selectors to schedule Pods](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)
## [Understand the role of DaemonSets](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
## [Understand how resource limits can affect Pod schceduleing](https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/)
## Manually schedule a pod without a scheduler
* [Assign a Pod to a Node with a nodeSelector](https://kubernetes.io/docs/concepts/configuration/assign-pod-node/)
    ```yaml
    # pods/pod-nginx.yaml 

    apiVersion: v1
    kind: Pod
    metadata:
      name: nginx
      labels:
        env: test
    spec:
      containers:
      - name: nginx
        image: nginx
        imagePullPolicy: IfNotPresent
      # assign pod to a node with disktype=ssd
      nodeSelector:
        disktype: ssd
    ```
## Display scheduler events
* Get the events from the Events api-resource

    ```sh
    kubectl get events
    ```
* Describe the pod

    ```sh
    kubectl describe pods <pod under investigation> | grep -A7 ^Events
    ```

## Know how to configure the Kubernetes scheduler