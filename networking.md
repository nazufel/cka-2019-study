# Networking 11%

## [Understand the networking configuration on the cluster nodes](https://kubernetes.io/docs/concepts/cluster-administration/networking/)
## Understand Pod networking concepts
    - Youtube: [The ins and outs of networking in Google Container Engine and Kubernetes (Google Cloud Next '17)](https://www.youtube.com/watch?v=y2bhV81MfKQ)
    - [Kubernetes Network Model](https://speakerdeck.com/thockin/illustrated-guide-to-kubernetes-networking?slide=7)
    - Youtube: [Networking with Kubernetes](https://www.youtube.com/watch?v=WwQ62OyCNz4)
    - [Illustrated guide to networking by Tim Hockin](https://speakerdeck.com/thockin/illustrated-guiernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/) 
## Understand service networking
    - Youtube; [Life of a paker](https://www.youtube.com/watch?v=0Omvgd7Hg1I)
## [Deploy and configure network load balancer](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)
## [Know how to use Ingress rules](https://kubernetes.io/docs/concepts/services-networking/ingress/)
## [Know how to configure and use the cluster DNS](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pod-s-dns-config)
## [Undrstand CNI](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/#cni)