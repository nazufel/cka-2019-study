# To Study or Practice

Stuff that I need to study harder and actually practice.

* [Configure secure cluster communications](https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/)
* [Configure a Highly-Available Kubernetse cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/)
* [Run end-to-end tests on your cluster](https://github.com/kubernetes/test-infra/blob/master/kubetest/README.md)
* [All of the security things](./security.md)
* [Understand CNI](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/#cni)