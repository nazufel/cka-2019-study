# Security 12% 

## Know how to configure authentication and authorization
* [Authentication](https://kubernetes.io/docs/reference/access-authn-authz/authentication/)
## Understand Kubernetes security primitives
* [Pod Security Policy](https://kubernetes.io/docs/concepts/policy/pod-security-policy/)
* [RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)
## [Know to configure network policies](https://kubernetes.io/docs/tasks/administer-cluster/declare-network-policy/)
## [Create and manage TLS certifiates for cluster components](https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/)
## Work with images securely
## [Define security contexts](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)
## [Secure persistent key value stores](https://kubernetes.io/docs/concepts/configuration/secret/)